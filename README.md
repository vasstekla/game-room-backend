# AccesaStart

What do you need to run the application:
1.MongoDB (Add the installed bin folder to PATH (default: C:\ProgramFiles\MongoDB\Server\3.4\bin))
2..Net

How to run the application:
1. Clone the repository to your computer
2. Open the Gamification/Gamification.sln project, and start it
3. The port will be listed in the command window where the server listens.
4. Start the react project, by going to accesambition folder, and running the following commands: npm installed (wait for it to finish), npm start

Login with: username: banana and password: banana

Implemented AI: Click on the robot on the right corner, and say for example: "go to leaderboard". 
				Also try saying: "play darts with orange", or "add new team with honey". The text you said should appear below the robot.
				By clicking the text you can trigger the event.
				
Implemented Score tracking by game: dart or foosball, you can try it out by accessing them via sidebar.

Implemented the feedback.
				
Implemented LinkedIn share, although it doesn't work properly, and I question why.

The other features are already presented :)