﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gamification.DataLayer;
using Gamification.DataModel;
using Gamification.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Gamification.Controllers
{
    
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        // GET: api/Game
        // Possible query parameters: date (YYYY-MM-DD), type (Darts or Foosball)
        [HttpGet]
        [Route("api/games")]
        public async Task<IActionResult> Get()
        {
           return new ObjectResult(await _gameRepository.GetAllGames());
        }


        // POST: api/Game
        [Route("api/game")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Game game)
        {          
            await _gameRepository.Insert(game);
            return new OkObjectResult(game);
        }

    }
}
