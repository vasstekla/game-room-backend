﻿using Gamification.DataModel;
using Gamification.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Context
{
    public class ContextImplementation : IContext
    {
        private readonly IMongoDatabase _db;

        public ContextImplementation(IOptions<DbSettings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _db = client.GetDatabase(options.Value.Database);
        }

        public IMongoCollection<Game> GameCollection => _db.GetCollection<Game>("games");

        public IMongoCollection<User> UserCollection => _db.GetCollection<User>("users");

        public IMongoCollection<Team> TeamCollection => _db.GetCollection<Team>("teams");
    }
}
