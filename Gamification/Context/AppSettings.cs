﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Context
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
