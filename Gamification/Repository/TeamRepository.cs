﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gamification.Context;
using Gamification.DataModel;
using Gamification.Model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Gamification.Repository
{
    public class TeamRepository : ITeamRepository
    {

        private readonly IContext _context;

        public TeamRepository(IContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            return await _context.TeamCollection.Find(_ => true).ToListAsync();
        }

        public async Task<IEnumerable<Team>> GetAllTeamsWithPaginate(int pageSize, int pageNumber)
        {
            var filter = Builders<Team>.Filter.Eq<bool>(t => t.IsSingleUser, false);
            return await _context.TeamCollection.Find(filter).Skip(pageSize * (pageNumber - 1)).Limit(pageSize).ToListAsync();
        }

        public async Task<Team> GetById(ObjectId Id)
        {
            var filter = Builders<Team>.Filter.Eq<ObjectId>(u => u.Id, Id);
            return await _context.TeamCollection.FindSync(filter).FirstOrDefaultAsync();
        }

        public async Task Insert(string teamname, string memberid, LinkedList<TeamScore> scores, bool isSingleUser)
        {
            var getid = ObjectId.Parse(memberid);
            LinkedList<ObjectId> players = new LinkedList<ObjectId>();
            players.AddLast(getid);
            await _context.TeamCollection.InsertOneAsync(new Team(teamname, players, scores ,isSingleUser));
        }

        public async Task Insert(Team team)
        {
            await _context.TeamCollection.InsertOneAsync(team);
        }

        public async Task<bool> UpdateName(string teamName, string name)
        {
            var filter = Builders<Team>.Filter.Eq<string>(t => t.Name, teamName);
            Team team = await _context.TeamCollection.FindSync(filter).FirstOrDefaultAsync();
            team.Name = name;
            ReplaceOneResult updateResult = _context.TeamCollection.ReplaceOne(filter, team);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> UpdateScore(ObjectId Id, TeamScore teamScore)
        {
            var filter = Builders<Team>.Filter.Eq<ObjectId>(t => t.Id, Id);
            Team team = await _context.TeamCollection.FindSync(filter).FirstOrDefaultAsync();
            foreach(TeamScore teamScoreElement in team.Scores)
            {
                if(teamScoreElement.GameType == teamScore.GameType)
                {
                    teamScoreElement.Score += teamScore.Score;
                    break;
                }
            }
            
            ReplaceOneResult updateResult = _context.TeamCollection.ReplaceOne(filter, team);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }


    }
}
