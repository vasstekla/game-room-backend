﻿using Gamification.DataModel;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Repository
{
    public interface IUserRepository
    {
        Task Insert(User user);

        Task<IEnumerable<User>> GetAllUser();

        Task<User> Authenticate(User user);

        Task<bool> Update(ObjectId Id, User user);

        Task<bool> Delete(ObjectId Id);

        Task<User> GetById(ObjectId Id);

        Task<User> GetByUsername(String username);
    }
}
