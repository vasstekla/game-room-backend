﻿using Gamification.DataModel;
using Gamification.Model;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Repository
{
    public interface ITeamRepository
    {
        Task<IEnumerable<Team>> GetAllTeams();

        Task<IEnumerable<Team>> GetAllTeamsWithPaginate(int pageSize, int pageNumber);

        Task Insert(string teamname, string memberid, LinkedList<TeamScore> scores, bool isSingleUser);
  
        Task Insert(Team team);

        Task<bool> UpdateScore(ObjectId Id, TeamScore teamScore);

        Task<bool> UpdateName(string teamName, string name);

        Task<Team> GetById(ObjectId Id);
    }
}
