﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.DataModel
{
    public class User
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Username { get; set; }
        public String EmailAdress { get; set; }
        public String Password { get; set; }
        public String Token { get; set; }
        public String ProfilePicture { get; set; }

        [JsonConstructor]
        public User(string id, string firstName, string lastName, string username, string emailAdress, string password, string token, string profilePicture)
        {
            if(id != null)
            {
                Id = ObjectId.Parse(id);
            }
            
            FirstName = firstName;
            LastName = lastName;
            Username = username;
            EmailAdress = emailAdress;
            Password = password;
            Token = token;
            ProfilePicture = profilePicture;
        }

        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public User(string firstName, string lastName, string username, string emailAdress, string password, string token, string profilePicture)
        {
            FirstName = firstName;
            LastName = lastName;
            Username = username;
            EmailAdress = emailAdress;
            Password = password;
            Token = token;
            ProfilePicture = profilePicture;
        }

    }
}
